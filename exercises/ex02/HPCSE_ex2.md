﻿# HPCSE Exercise sheet 2

## Question 1: cache size and speed

### Task a)
Obtained with the command:

    bsub -R "select[model==XeonE3_1585Lv5]" -n 1 "lscpu && grep . /sys/devices/system/cpu/cpu0/cache/index*/*"

- L1 cache (data & instruction):
	- Line size: 64B
	- Cache size: 32KB
	- Associativity: 8-way
- L2 cache:
	- Line size: 64B
	- Cache size: 256KB
	- Associativity: 4-way
- L3 cache:
	- Line size: 64B
	- Cache size: 8192KB
	- Associativity: 16-way

### Tasks b) c) d)
![Performance on Euler](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex02/skeleton_code/1_cache_size/euler_out/results.png)
The random permutation method shows a decay in performance, but not sharp steps. The soft steps are around the different cache sizes: 32, 256 and 8192 KB.

For sequential access, no decay occurs. This is the most efficient way to use memory, optimizing operations per memory call. Only every 8th integer call is pulling a new cache line. I'd guess the scheduler uses these extra steps to "preload" the upcoming cachelines, meaning we effectively are always using L1 cache.

For sequential access in steps of 64B, there is a very sharp step around 32KB. This is because once we exceed the L1d cache size, _every step_ is calling a new cache line from L2 cache. This effectively limits our memory access time to that of the L2 cache, negating the advantage of the L1 cache.


## Question 2: Cache associativity

 ### Task a)
 See 1a)
 
 ### Task b)
 ![Performance as a function of the number of arrays K](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex02/skeleton_code/2_associativity/euler_out/results.png)
 With no padding, the first big performance hit comes around $K = 4$, which is the cache associativity of the L2  cache. At $K = 8$ we have an even faster deterioration, when we exceed the associativity of the L1 cache.

With 64B padding, the effect is greatly reduced, since we are "out of step" with the cache.

   ![](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex02/skeleton_code/2_associativity/euler_out/results2.png)

Out of curiosity, I implemented a third version with the nested loops reordered, i.e. incrementing every item in one list before going to the next one. This is much faster, since the memory is being accessed in sequence.


## Question 3: Anderson Lock
![Plotted results form Euler](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex02/skeleton_code/3_alock/euler_out/results.png)

Mutual exclusion is respected: only one thread at a time runs the critical section. Furthermore, FIFO-fairness is also demonstrated.

Assuming $n$ concurrent threads all performing the same amount of work inside ($W_i$) and outside ($W_o$), and a negligible time between release and acquisition of the lock, the percentage of time spent waiting for the lock is:

$$t_{Waste} = \frac{(n-1) \cdot W_i - W_o}{n \cdot W_i}$$

In the case where $W_i \approx W_o$, this amounts to

$$t_{Waste} = \frac{n-2}{n} = 1 - \frac{2}{n} \stackrel{n = 8}{=} 0.75$$

The higher the number of cores, the higher the percentage of wasted time.

If, however, we decrease the amount of work done in the concurrent section such that $W_o \approx 10\cdot W_i$, the amount of wasted time will practically disappear, as long as we have $11$ threads or fewer. The reason is that $W_o$ is greater than the combined critical sections of other $10$ threads, meaning by the time the first thread has finished $W_o$ and requests the lock again, the others will be outside the critical section. If, however, we have more than $11$ threads, the behaviour is similar, and depends once more on $n$:

$$t_{Waste} = 1 - \frac{11}{n} \stackrel{n = 16}{=} 0.3125$$
