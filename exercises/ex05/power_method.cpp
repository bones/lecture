#include <iostream>
#include <random>
#include <chrono>
#include <cmath> // std::abs, std::sqrt
#include <algorithm> // std::copy
#include <memory> // std::unique_ptr

#include <cblas.h>

using real_t = double;
using my_clock_t = std::chrono::steady_clock;

/**
 * MODE=0 -> naive implementation
 * MODE=1 -> CBLAS implementation
 * MODE=3 -> LAPACK
**/

#ifndef MODE
#define MODE 0
#endif 

#ifndef SIZE
#define SIZE 32
#endif

#ifndef TOL
#define TOL 1e-12
#endif

#ifndef SEED
#define SEED 42
#endif


/**
 * Base C++ implementation of matrix-vector multiplication.
 *
 * Pre:
 *  - A is an NxN matrix in row-major
 *  - vec is a vector of size N
 *  
 * Post:
 *  - vec = A*vec
**/
void mat_vec(const real_t * A, real_t * vec, size_t N) {
    //real_t result[SIZE];
    real_t * result = new real_t[N];

    for (unsigned i = 0; i < N; i++) {
        real_t res = 0.;
        for (unsigned j = 0; j < N; j++) {
            res += A[i*N + j] * vec[j];
        }
        result[i] = res;
    }

    std::copy(result, result + N, vec);

    delete [] result;
}

/**
 * Compute 2-norm of a vector.
 *
 * Pre:
 * - vec is a vector of size N
 *
 * Post:
 * - return value is the euclidean norm of the input vector
**/
real_t norm(const real_t* const vec, size_t N) {
    real_t res = 0.;
    for (unsigned i = 0; i < N; i++) {
        res += vec[i] * vec[i];
    }
    return std::sqrt(res);
}

/**
 * Implementation of power method.
 *
 * Pre:
 *  - A is an NxN matrix in row-major
 *  - q is the initial guess vector of size N, norm(q) == 1
 *  - N is the size
 *  - tol is the tolerance used for termination
 *
 * Post:
 *  - return value is the number of iterations performed
 *  - q is the converged eigenvector
 *  - lambda is the converged eigenvalue
 *
**/
#if MODE == 1
// CBLAS implementation
unsigned int pm_cblas(const real_t * const A, real_t* q,
                    real_t& lambda, size_t N, const real_t& tol) {
#else
// Naive implementation
unsigned int pm_baseline(const real_t * const A, real_t* q,
                    real_t& lambda, size_t N, const real_t& tol) {
#endif

    real_t error = 0.;
    unsigned int num_iter = 0;
    lambda = 1.;

    do {
#if MODE == 1
        // CBLAS implementation
        /*
        void cblas_dgemv(const enum CBLAS_ORDER order,
                         const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                         const double alpha, const double *A, const int lda,
                         const double *X, const int incX, const double beta,
                         double *Y, const int incY);
        */

        real_t new_q[SIZE];
        cblas_dgemv(CblasRowMajor,
                    CblasNoTrans, N, N, 
                    1, A, N,
                    q, 1, 0,
                    new_q, 1);
        cblas_dswap(N, q, 1, new_q, 1);

        real_t new_lambda = cblas_dnrm2(N, q, 1);
#else
        // q = A*q
        mat_vec(A, q, N);

        // Since q is normed, the norm of A*q is the eigenvalue of q
        real_t new_lambda = norm(q, N);
#endif

        error = std::abs(new_lambda - lambda);

        // Normalize new q vector
        for (unsigned i = 0; i < N; i++) {
            q[i] /= new_lambda;
        }

        // Assign new value
        lambda = new_lambda;

        ++num_iter;
    } while (error >= tol);

    return num_iter;
}


int main() {
    const size_t N = SIZE;

    // Possible values of \alpha
    const size_t num_alphas = 9;
    const real_t alphas[] = {1./8, 1./4, 1./2, 1, 3./2, 2, 4, 8, 16};

    // For each value of \alpha, perform measurement
    for (unsigned a = 0; a < num_alphas; a++) {
        const real_t alpha = alphas[a];

        std::default_random_engine rng(SEED);
        std::normal_distribution<real_t> dist(0.0, 1.0);

        // Set up A matrix
        real_t A[N*N];
        for (unsigned i = 0; i < N; i++) {
            for (unsigned j = 0; j < N; j++) {
                if (i == j)
                    A[i*N + j] = alpha * i;
                else
                    A[i*N + j] = dist(rng);
            }
        }
    
        // Set up initial guess vector
        real_t q[N] = {0.0};
        q[0] = 1.;

        // Perform calculation, measure time
        auto t1 = my_clock_t::now();
#if MODE == 1
        // CBLAS implementation
#elif MODE == 2
        // LAPACK implementation
#else
        // Baseline implementation
        real_t lambda = 1;
        unsigned num_iter = pm_baseline(A, q, lambda, N, TOL);
#endif
        auto t2 = my_clock_t::now();
        const double time = 
            std::chrono::duration_cast<std::chrono::nanoseconds>
                (t2 - t1).count() / 1e9;

        // Output

#if MODE == 1
        // CBLAS implementation
#elif MODE == 2
        // LAPACK implementation
#else
        // Baseline implementation
        std::cout << "\n********************************\n" << std::endl;
        std::cout << "Alpha value:\t" << alpha << std::endl;
        std::cout << "Iterations:\t" << num_iter << std::endl;
        std::cout << "Runtime:\t" << time << std::endl;
        std::cout << "Eigenvalue:\t" << lambda << std::endl;
#endif
    } // end for(a)

}
