﻿# HPCSE Exercise Sheet 3

## Question 1: Parallel Monte Carlo using OpenMP

Results on Euler:
![Results of benchmarks on Euler](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex03/skeleton_code/results_euler.png)

Results on my PC:
![Results of benchmarks on my PC](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex03/skeleton_code/results_laptop.png)

The serial version of the program, of course, has roughly constant runtime.

The fully-OpenMP version and the version with padding have near-perfect scaling, with runtime almost halving when the number of cores doubles.

The version with no padding on my computer clearly suffers from false sharing, and runtime is negatively affected by this. The same code on Euler though, seems not to experience any such problem - I am not sure why. The processor used on Euler was an `Intel Xeon CPU E5-2680 v3`.

Because the work is being allocated to the threads by OpenMP reduction, not all threads do the same amount of work: one or some will have to sum up the single contributions of each individual thread. 

Numerical results from computations:

    $ ./main 0
    res:  3.14176444000000021362
    ref:  3.14159265358979311600
    error: 1.71786410207097617331e-04
    time: 2.25062498999977833591
    $ export OMP_NUM_THREADS=1
    $ ./main 1
    res:  3.14176444000000021362
    ref:  3.14159265358979311600
    error: 1.71786410207097617331e-04
    time: 2.26760500499949557707
    $ export OMP_NUM_THREADS=8
    $ ./main 1
    res:  3.14197543999999995279
    ref:  3.14159265358979311600
    error: 3.82786410206836791303e-04
    time: 0.36710665000100561883

Executing the program under the same conditions twice will yield the same result, as the random number generators are seeded to the same value on each iteration. For the same reason, executing the serial version or the OpenMP version with one thread yield the same result. However, running the program with one thread or multiple threads makes a difference, as the random number generators for each thread are seeded differently.

## Question 2: OpenMP bug hunting
Assuming `is_good` is thread-safe, the only issue I can see is the fact that `good_members` is shared. Therefore, could try to assign different values of `i` to the same index `pos`. The first solution would be to designate this assignment as a critical section:

    #define N 1000
    
    extern struct data member[N];
    extern int is_good(int i);
    
    int good_members[N];
    int pos = 0;
    
    void find_good_members() {
    #pragma omp parallel for
        for (int i = 0; i < N; i++){
            if (is_good(i)) {
    #pragma omp critical
                good_members[pos] = i;
    
    #pragma omp atomic
                pos++;
            }
        }
    }
    
The second would be to use an `atomic capture` or a critical section to perform both the assignment in the array and the increment of `pos`:

    #define N 1000
    
    extern struct data member[N];
    extern int is_good(int i);
    
    int good_members[N];
    int pos = 0;
    
    void find_good_members() {
    #pragma omp parallel for
        for (int i = 0; i < N; i++){
            if (is_good(i)) {
    #pragma omp atomic capture
                {
                good_members[pos] = i;
    
                pos++;
                } // end critical section
        }
    }

