#define N 1000

extern struct data member[N];
extern int is_good(int i);

int good_members[N];
int pos = 0;

void find_good_members() {
#pragma omp parallel for
    for (int i = 0; i < N; i++){
        if (is_good(i)) {
            good_members[pos] = i;

#pragma omp atomic
            pos++;
        }
    }
}








#define N 1000

extern struct data member[N];
extern int is_good(int i);

int good_members[N];
int pos = 0;

void find_good_members() {
#pragma omp parallel for
    for (int i = 0; i < N; i++){
        if (is_good(i)) {
#pragma omp critical
            good_members[pos] = i;

#pragma omp atomic
            pos++;
        }
    }
}








#define N 1000

extern struct data member[N];
extern int is_good(int i);

int good_members[N];
int pos = 0;

void find_good_members() {
#pragma omp parallel for
    for (int i = 0; i < N; i++){
        if (is_good(i)) {
#pragma omp atomic capture
            {
            good_members[pos] = i;

            pos++;
            } // end critical section
    }
}
