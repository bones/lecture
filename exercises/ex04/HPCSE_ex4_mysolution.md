﻿# HPCSE 1: exercise set 4
Sean Bone

## Question 1: Amdahl's Law
### Task a)
A $55$km bus ride takes $90$ minutes. If I stop in Rotkreuz instead, the bus ride becomes $55 - 36 = 19$km long, taking approximately $\frac{19}{55}\cdot 90 \approx 31$ minutes. The train ride from Rotkreuz to Zürich lasts $\frac{36 \text{ km}}{70 \text{ km/h}} \approx 31$ minutes. The entire ride now lasts $62$ minutes, making for a speed-up $S = \frac{90}{62} \approx 1.45$x .
If the trains were to travel at the speed of light, the train trip would be almost instantaneous. In general, $\lim_{v\to\infty} T = 31$ minutes, i.e. $\lim_{v\to\infty} S = \frac{90}{31} \approx 2.9$x .

### Task b)

1. $f_1 = 0.1$, $f_p = 0.9$, $S_p \stackrel{!}{=} 8$x . Reworking Amdahl's law:
$$p = \frac{f_p}{\frac{1}{S_p} - f_1} = \frac{0.9}{\frac{1}{8} - 0.1} = 36 \text { cores needed}$$
2. We need $36$ processors to achieve $8$x speed-up, but we only have $24$ available. This would mean we need to buy another node, bringing us to $48$ nodes. The speed-up would then be:
$$S_{48} = \frac{1}{f_1 + \frac{f_p}{48}} \approx 8.4\text{x}$$
3. At a certain point, you get diminishing returns. Assuming perfect scaling, the maximum speed-up would be tenfold. Seeing as we get diminishing returns for our investment it would probably not make sense to buy another node.
$$S_{72} \approx 8.9\text{x} \qquad S_{96} \approx 9.14\text{x} \qquad \lim_{p\to\infty} S_p = 10\text{x}$$

## Question 2: Manual Vectorization of Reduction Operator

### Task c)
i) The expected speedup for 32bit and 64bit precision is 4x and 2x respectively. This is also achieved in the implementation.

ii) For both single and double precision, the single-thread speedup is correct, but the program scales worse with the larger array than with the smaller one. My initial guess would be that the larger array means each thread is having to fetch more data from higher-level memory, causing more bus traffic and therefore longer wait times for each thread.

`lscpu` indicates an `Intel Xeon E5-2680 v3` processor was used, with $32$KB L1 cache, $256$KB L2 cache and $30$MB L3 cache. Therefore:

- For single precision numbers:
    - Small N: $N_0 = 2^{15} = 32,768$ items $= 131,072$ bytes $= 128$KB $\rarr$ fits into L2 cache.
    - Big N: $N_1 = 2^{20} = 1,048,576$ items $= 4,194,304$ bytes $= 4,096$KB $\rarr$ only fits into L3 cache.

- For double  precision numbers:
    - Small N: $N_0 = 2^{15} = 32,768$ items $= 262,144$ bytes $= 256$KB $\rarr$ fits into L2 cache.
    - Big N: $N_1 = 2^{20} = 1,048,576$ items $= 8,388,608$ bytes $= 8,192$KB $\rarr$ only fits into L3 cache.

This confirms that the larger arrays require each thread to query the L3 cache, which is shared among all CPU cores, as opposed to the L2 cache. This causes more bus traffic and effectively increases memory latency.

4-way SSE (single precision):
![4-way SSE (single precision)](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex04/skeleton_code/vectorized_reduction/76198190speedup_4-way.png)


2-way SSE (double precision):
![2-way SSE (double)](https://gitlab.ethz.ch/bones/lecture/raw/master/exercises/ex04/skeleton_code/vectorized_reduction/76198190speedup_2-way.png)

## Question 3: Intel SPMD Program Compiler (ISPC)

### Tasks a) - c)
The baseline implementation works and is correct. The SPMD  implementation works and is correct, but is slower than the baseline. I'm not sure why.

### Task d)
SSE2 operates on 4 floats or 2 doubles in parallel, so we expect a speedup of 4x and 2x respectively.
AVX2 has wider registers, operating on 8 floats or 4 doubles in parallel. Therefore we expect speedups of 8x and 4x respectively.
I tried many different implementations, but could never achieve these speedups. Eventually I adapted the solution from the [ISPC examples section](https://github.com/ispc/ispc/blob/master/examples/sgemm/SGEMM_kernels.ispc), but even that is slower than the baseline.

### Tasks e) - f)
The SSE2 implementation has no error.
The AVX2 implementation has an error of around `2e-14`. I assume this is due to the Fast Multiply-Add operations that were introduced in AVX, which, [according to Wikipedia](https://en.wikipedia.org/wiki/Multiply%E2%80%93accumulate_operation#Fused_multiply%E2%80%93add), are faster and slightly more accurate than traditional, unfused, multiply-add operations. This is because rounding only happens once per FMA operation, as opposed to twice (once for the multiplication, once for the sum).
