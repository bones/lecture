// File       : gemm.ispc
// Created    : Wed Oct 17 2018 09:43:19 AM (+0200)
// Description: ISPC GEMM body
// Copyright 2018 ETH Zurich. All Rights Reserved.
#include "common.h"

/**
 * @brief General matrix-matrix multiplication kernel (GEMM). Computes C = AB.
 * ISPC implementation used for SSE2 and AVX2 extended ISA
 *
 * @param A Matrix dimension p x r
 * @param B Matrix dimension r x q
 * @param C Matrix dimension p x q
 * @param p Dimensional parameter
 * @param r Dimensional parameter
 * @param q Dimensional parameter
 */
/* TODO: Missing piece */
#ifdef _ISPC_SSE2_
export void gemm_sse2(
#else
export void gemm_avx2(
#endif /* _ISPC_SSE2_ */
        const uniform Real* const uniform A,
        const uniform Real* const uniform B,
        uniform Real* const uniform C,
        const uniform int p,
        const uniform int r,
        const uniform int q)
{
    ///////////////////////////////////////////////////////////////////////////
    // TODO: Write your ISPC implementation of a matrix-matrix multiplication
    // here.  Try to use a general type for floating point numbers, such that
    // you can easily compile this kernel for float (single precision, 32bit)
    // and double (double precision, 64bit).  Check out the 'common.h' header
    // for a hint.  Note: A working code can be implemented with ~15 lines of
    // ISPC code.
    //
    // We use two different function names for the same function, such that we
    // can compile for two different targets (SSE2 and AVX2) and link both of
    // them to our application code in 'gemm.cpp'.
    ///////////////////////////////////////////////////////////////////////////
    
    /*
     // Iterate over A
    for (int i = 0; i < p; i++) {
        for (int j = 0; j < r; j++) {
            const T& aij = A[j + r*i];
            // Iterate over j-th row of B
            for (int k = 0; k < q; k++) {
                const T& bjk = B[k + q*j];
                C[k + q*i] += aij * bjk;
            }
        }
    }
    */
    
    /*foreach (i = 0 ... p, j = 0 ... r, k = 0 ... q) {
        C[k + q*i] += A[j + r*i] * B[k + q*j];
    }*/

    for (int i = 0; i < p; i++) {
        for (int j = 0; j < r; j++) {
            const uniform Real a = A[j + r*i];
            // Iterate over j-th row of B
            foreach (k = 0 ... q) {
                C[k + q*i] += a * B[k + q*j];
            }
        }
    }
}
