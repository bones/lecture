#include <cstdio>
#include <chrono>

#include <iostream>

void measure_flops(int N, int K, bool fast=false) {
    // Question 2b: Allocate the buffer of N * K elements.
    
    double buffer[K][N];

    for (int j = 0; j < K; j++)
        for (int i = 0; i < N; i++)
            buffer[j][i] = 0.;

    // Question 2b: Repeat `repeat` times a traversal of arrays and
    //                    measure total execution time.
    int repeat = 500 / K;

    // Set up clock
    using my_clock_t = std::chrono::high_resolution_clock;
    std::chrono::time_point< my_clock_t > start , stop;

    if (!fast) {
        start = my_clock_t::now();
        for (int rep = 0; rep < repeat; rep++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < K; j++) {
                    buffer[j][i] += .5;
                }
            }
        }
        stop = my_clock_t::now();
    } else {
        start = my_clock_t::now();
        for (int rep = 0; rep < repeat; rep++) {
            for (int j = 0; j < K; j++) {
                for (int i = 0; i < N; i++) {
                    buffer[j][i] += .5;
                }
            }
        }
        stop = my_clock_t::now();
    }


    // Question 2b: Deallocate.


    // Report.
    double time = static_cast<std::chrono::duration<double> >(stop-start).count();
    double flops = (double)repeat * N * K / time;
    printf("%d  %2d  %.4lf\n", N, K, flops * 1e-9);
    fflush(stdout);
}

void run(int N, bool fast=0) {
    printf("      N   K  GFLOPS\n");
    for (int K = 1; K <= 40; ++K)
        measure_flops(N, K, fast);
    printf("\n\n");
}

int main() {
    // Array size. Must be a multiple of a large power of two.
    const int N = 1 << 20;

    // Power of two size --> bad.
    run(N);

    // Non-power-of-two size --> better.
    // Enable for Question 2c:
    run(N + 64 / sizeof(double));

    run(N, true);
    
    // NOTE (2018-10-09): If running on Euler II nodes, try larger paddings such as 2, 4, 8, 16 cache lines.

    return 0;
}

