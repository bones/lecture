#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <omp.h>

#define RNG_SEED 0

// Integrand
inline double F(double x, double y) {
  if (x * x + y * y < 1.) { // inside unit circle 
    return 4.; 
  }
  return 0.;
}

// Method 0: serial
double C0(size_t n) {
  // random generator with seed 0
	std::default_random_engine g(RNG_SEED); 
  // uniform distribution in [0, 1]
	std::uniform_real_distribution<double> u; 

	double s = 0.; // sum
	for (size_t i = 0; i < n; ++i) {
		double x = u(g);
		double y = u(g);
		s += F(x, y);
	}
  return s / n;
}

// Method 1: openmp, no arrays 
// Question 1a.1
double C1(size_t n) {
  // random generator with seed 0

	double s = 0.; // sum
#pragma omp parallel
  {
	// Start a different rng for each thread, and give them different seeds
	unsigned tid = omp_get_thread_num();
	std::default_random_engine g(RNG_SEED + tid); 
	std::uniform_real_distribution<double> u; 

#pragma omp for reduction(+: s)
	for (size_t i = 0; i < n; ++i) {
		double x = u(g);
		double y = u(g);
		s += F(x, y);
	}
  }
  return s / n;
}


// Method 2, only `omp parallel for reduction`, arrays without padding
// Question 1a.2
double C2(size_t n) {
	double s = 0.; // sum

	// Start a different rng for each thread, and give them different seeds
	unsigned max_threads = omp_get_max_threads();
	std::default_random_engine engines[max_threads];

	for (unsigned i = 0; i < max_threads; i++)
		engines[i].seed(RNG_SEED + i); 

	std::uniform_real_distribution<double> u;

#pragma omp parallel for reduction(+: s)
	for (size_t i = 0; i < n; ++i) {
		auto& g = engines[omp_get_thread_num()];
		double x = u(g);
		double y = u(g);
		s += F(x, y);
	}
  return s / n;
}

// Method 3, only `omp parallel for reduction`, arrays with padding
// Question 1a.3
double C3(size_t n) {
	double s = 0.; // sum

	// Start a different rng for each thread, and give them different seeds
	unsigned max_threads = omp_get_max_threads();
	size_t sz = sizeof(std::default_random_engine);
	unsigned step = 64 / sz + 1;
	std::default_random_engine engines[max_threads * step];

	for (unsigned i = 0; i < max_threads; i++)
		engines[i * step].seed(RNG_SEED + i); 

	std::uniform_real_distribution<double> u;

#pragma omp parallel for reduction(+: s)
	for (size_t i = 0; i < n; ++i) {
		auto& g = engines[step * omp_get_thread_num()];
		double x = u(g);
		double y = u(g);
		s += F(x, y);
	}
  return s / n;
}

// Returns integral of F(x,y) over unit square (0 < x < 1, 0 < y < 1).
// n: number of samples
// m: method
double C(size_t n, size_t m) {
  switch (m) {
    case 0: return C0(n);
    case 1: return C1(n); 
    case 2: return C2(n); 
    case 3: return C3(n); 
    default: printf("Unknown method '%ld'\n", m); abort();
  }
}


int main(int argc, char *argv[]) {
  // default number of samples
  const size_t ndef = 1e8;

  if (argc < 2 || argc > 3 || std::string(argv[1]) == "-h") {
    fprintf(stderr, "usage: %s METHOD [N=%ld]\n", argv[0], ndef);
    fprintf(stderr, "Monte-Carlo integration with N samples.\n\
METHOD:\n\
0: serial\n\
1: openmp, no arrays\n\
2: `omp parallel for reduction`, arrays without padding\n\
3: `omp parallel for reduction`, arrays with padding\n"
        );
    return 1;
  } 

  // method
  size_t m = atoi(argv[1]);
  // number of samples
	size_t n = (argc > 2 ? atoi(argv[2]) : ndef);
  // reference solution
  double ref = 3.14159265358979323846; 

	double wt0 = omp_get_wtime();
  double res = C(n, m);
	double wt1 = omp_get_wtime();

	printf("res:  %.20f\nref:  %.20f\nerror: %.20e\ntime: %.20f\n", 
      res, ref, res - ref, wt1 - wt0);

	return 0;
}
