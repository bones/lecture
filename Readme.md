
# TA Contacts

* Fabian (Head TA): <fabianw@mavt.ethz.ch>
* Athena:           <eceva@ethz.ch>
* Guido:            <novatig@ethz.ch>
* Ivica:            <kicici@ethz.ch>
* Petr:             <kpetr@ethz.ch>
* Renato:           <renatob@student.ethz.ch>

# Exercise Hand-in

Please submit your exercise solutions via Moodle (nethz login required)

[EXERCISE SUBMISSION](https://moodle-app2.let.ethz.ch/course/view.php?id=5072)

# Lecture Schedule

| When       | Who  | What                                                 |
|------------|------|------------------------------------------------------|
| 2018-09-21 | PK   | _Introduction, Architectures, Performance_           |
| 2018-09-28 | FW   | _Shared Memory, Concurrency, Threads_                |
| 2018-10-05 | FW   | _OPEN-MP 1_                                          |
| 2018-10-12 | FW   | _OPEN-MP 2, Pipelining and Vectorization_            |
| 2018-10-19 | LA   | _HPC Libraries and Linear Algebra_                   |
| 2018-10-26 | PK   | _SVD and Neural Networks_                            |
| 2018-11-02 | PK   | _Neural Networks as Matrix-Vector Multiply_          |
| 2018-11-09 | LA   | _Distributed Memory, MPI 1_                          |
| 2018-11-16 | LA   | _MPI 2_                                              |
| 2018-11-23 | LA   | _MPI 3, Strong/Weak Scaling_                         |
| 2018-11-30 | PK   | _Diffusion: Structured Grids and Finite Differences_ |
| 2018-12-07 | PK   | _Diffusion: Particles and N-Body Solvers_            |
| 2018-12-14 | PK   | _Linked Lists and Particle-Mesh Operations_          |
| 2018-12-21 |      | _TBD_                                                |
| 2019-01-11 | EXAM |                                                      |

# Exam

## Allowed Documents

You are allowed to bring a **handwritten** summary of 4 A4 sheets, written on
the front and back pages (8 pages total).  In addition, we will provide you
with the following material:

1. All lecture slides
2. All exercise solutions
3. Lecture notes that are distributed in addition to the slides
4. Manuals / references:
    * OpenMP 4.5 specification
    * OpenMP 4.5 reference card
    * MPI 3.1 standard
    * MPI reference card
    * cppreference webpage (offline version)
    * Intel intrinsics guide (offline version)
    * The book by Victor Eijkhout

The items in 4. have already been added to the class git repository.

# Frequently Asked Questions (FAQ)

Please have a look at these [Frequently asked questions](./faq/faq.md)
