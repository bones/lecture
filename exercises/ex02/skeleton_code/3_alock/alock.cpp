#include <atomic>
#include <chrono>
#include <omp.h>
#include <random>
#include <thread>

#include <cstdlib> // std::rand()
#include <iostream> // std::cout

const int MAX_T = 100;  // Maximum number of threads.

using my_clock_t = std::chrono::high_resolution_clock;
using time_point_t = std::chrono::time_point< my_clock_t >;

class ALock {
private:
    // Question 3a: Member variables.
    std::atomic<int> tail;
    volatile bool flag[MAX_T];
    int slot[MAX_T];

public:
    ALock() {
        // Question 3a: Initial values.
        tail = 0;
        flag[0] = 1;
        for (int i = 1; i < MAX_T; i++)
            flag[i] = 0;
    }

    void lock(int tid) {
        // Question 3a
        int s = tail.fetch_add(1) % MAX_T;
        slot[tid] = s;
        while (flag[s] == false)
            continue;
    }

    void unlock(int tid) {
        // Question 3a
        flag[slot[tid]] = false;
        flag[(slot[tid] + 1) % MAX_T] = true;
    }
};


/*
 * Print the thread ID and the current time.
 */
ALock log_lock;

void log(int tid, const char *info, time_point_t start) {
    // Question 3a: Print the event in the format:
    //  tid     info    time_since_start
    //
    // Note: Be careful with a potential race condition here.
    auto now = my_clock_t::now();
    auto time = static_cast<std::chrono::duration<double>>(now - start).count();

    log_lock.lock(tid);

    std::cout << tid << "\t" << info << "\t" << time << "\n";

    log_lock.unlock(tid);
}


/*
 * Sleep for `ms` milliseconds.
 */
void suspend(int ms) {
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}


void emulate() {
    ALock lock;
    std::srand(1);

    time_point_t start = my_clock_t::now();;

#pragma omp parallel
    {
        // Begin parallel region.
        int tid = omp_get_thread_num();  // Thread ID.

        // Question 3b: Repeat multiple times
        //      - log(tid, "BEFORE")
        //      - lock
        //      - log(tid, "INSIDE")
        //      - Winside
        //      - unlock
        //      - log(tid, "AFTER")
        //      - Woutside
        //
        // NOTE: Make sure that:
        //      - there is no race condition in the random number generator
        //      - each thread computes different random numbers

        for (int i = 0; i < 5; i++) {
            log(tid, "BEFORE", start);
            lock.lock(tid);

            log(tid, "INSIDE", start);

            int ms1 = 50 + 150 * (std::rand()*1./RAND_MAX);
            suspend(ms1);

            int ms2 = 50 + 150 * (std::rand()*1./RAND_MAX);

            lock.unlock(tid);
            log(tid, "AFTER", start);

            suspend(ms2);
        }
        // End parallel region.
    }
}


/*
 * Test that a lock works properly by executing some calculations.
 */
void test_alock() {
    const int N = 1e6, A[2] = {2, 3};
    int result = 0, curr = 0;
    ALock lock;

#pragma omp parallel for
    for (int i = 0; i < N; ++i) {
        int tid = omp_get_thread_num();  // Thread ID.
        lock.lock(tid);

        // Something not as trivial as a single ++x.
        result += A[curr = 1 - curr];

        lock.unlock(tid);
    }

    int expected = (N / 2) * A[0] + (N - N / 2) * A[1];
    if (expected == result) {
        fprintf(stderr, "Test OK!\n");
    } else {
        fprintf(stderr, "Test NOT OK: %d != %d\n", result, expected);
        exit(1);
    }
}


int main() {
    test_alock();

    // Question 3b:
    emulate();

    return 0;
}
