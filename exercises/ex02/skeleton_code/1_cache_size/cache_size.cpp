#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <iostream>

const int MIN_N = 1000 / sizeof(int);      // From 1 KB
const int MAX_N = 20000000 / sizeof(int);  // to 20 MB.
const int NUM_SAMPLES = 100;
const int M = 100000000;    // Operations per sample.
int a[MAX_N];               // Permutation array.

// Set up clock
using my_clock_t = std::chrono::high_resolution_clock;
std::chrono::time_point< my_clock_t > start , stop;


void sattolo(int *p, int N) {
    /*
     * Generate a random single-cycle permutation using Satollo's algorithm.
     *
     * https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Sattolo's_algorithm
     * https://danluu.com/sattolo/
     */
    for (int i = 0; i < N; ++i)
        p[i] = i;
    for (int i = 0; i < N - 1; ++i)
        std::swap(p[i], p[i + 1 + rand() % (N - i - 1)]);
}

double measure(int N, int mode) {

    int list[N];

    if (mode == 0) {
        // Question 1b: Use the sattolo function to generate a random one-cycle permutation.
        sattolo(list, N);

    } else if (mode == 1) {
        // Question 1c: Initialize the permutation such that k jumps by 1 item every step (cyclically).
        std::cout << N << std::endl;

        for (int i = 0; i < N; ++i)
            list[i] = i + 1;
        for (unsigned i = 0; i < N - 1; )
            list[i] = ++i;
        list[N-1] = 0;

    } else if (mode == 2) {
        // Question 1d: Initialize the permutation such that k jumps by 64 bytes (cyclically).
        // An in occupies 32 bits = 4 bytes -> jump 16 elements to jump 64 bytes
        int k = 0;
        for (int i = 0; i < N; ++i) {
            int oldk = k;

            k += 16;
            k %= N;
            list[oldk] = k;
        }
    }

    // Question 1b: Traverse the list (make M jumps, starting from k = 0) and measure the execution time.

    int k = 0;
    volatile int tmp;
    start = my_clock_t::now();
    for (int i = 0; i < M; ++i) {
        k = list[k];
        tmp = k; // Assign to volatile to prevent compiler from optimizing entire loop away
    }
    stop = my_clock_t::now();


    // Question 1b: Return execution time in seconds.
    const double time = static_cast<std::chrono::duration<double> >(stop-start).count();
    return time;
}

void run_mode(int mode) {
    /*
     * Run the measurement for many different values of N and output in a
     * format compatible with the plotting script.
     */
    printf("%9s  %9s  %7s  %7s\n", "N", "size[kB]", "t[s]", "op_per_sec[10^9]");
    for (int i = 0; i < NUM_SAMPLES; ++i) {
        // Generate N in a logarithmic scale.
        int N = (int)(MIN_N * std::pow((double)MAX_N / MIN_N,
                                       (double)i / (NUM_SAMPLES - 1)));
        double t = measure(N, mode);
        printf("%9d  %9.1lf  %7.5lf  %7.6lf\n",
               N, N * sizeof(int) / 1024., t, M / t * 1e-9);
        fflush(stdout);
    }
    printf("\n\n");
}

int main() {
    // Question 1b:
    run_mode(0);   // Random.

    // Enable for Question 1c: 
    run_mode(1);   // Sequential (jump by sizeof(int) bytes).  

    // Enable for Question 1d:
    run_mode(2);   // Sequential (jump by cache line size, i.e. 64 bytes).

    return 0;
}

